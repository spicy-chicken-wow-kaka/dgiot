%%--------------------------------------------------------------------
%% Copyright (c) 2020-2021 DGIOT Technologies Co., Ltd. All Rights Reserved.
%%
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%%--------------------------------------------------------------------
-module(dgiot_gb26875).
-author("stoneliu").
-include_lib("dgiot_gb26875.hrl").
-include_lib("dgiot/include/logger.hrl").
-export([
    sysdevice/3,
    equdevice/3
]).

sysdevice(#{<<"systype">> := SysType, <<"sysaddr">> := SysAddr} = Map, #{<<"source">> := Ip}, #state{devtype = DevType, app = App}) ->
    CategoryId = dgiot_parse:get_categoryid(1, <<"智慧消防"/utf8>>),
    ProductId = dgiot_parse:get_productid(CategoryId, DevType, SysType),
    Acl = #{<<"ACL">> => #{<<"role:",App/binary>> => #{<<"read">> => true, <<"write">> => true}}},
    case dgiot_product:local(ProductId) of
        {ok, _Product} ->
            pass;
        _ ->
            Prodocut = #{
                <<"name">> => SysType,
                <<"devType">> => DevType,
                <<"desc">> => SysType,
                <<"nodeType">> => 0,
                <<"ACL">> => Acl,
                <<"netType">> => <<"CELLULAR">>,
                <<"category">> => #{<<"objectId">> => CategoryId, <<"__type">> => <<"Pointer">>, <<"className">> => <<"Category">>},
                <<"thing">> => #{},
                <<"productSecret">> => dgiot_utils:random(),
                <<"dynamicReg">> => true},
            case dgiot_parse:create_object(<<"Product">>,Prodocut) of
                {ok, #{<<"objectId">> := _ObjectId}} ->
                    dgiot_product:save(Prodocut);
                _ -> pass
            end
    end,
    DeviceId = dgiot_parse:get_deviceid(ProductId, SysAddr),
    case dgiot_device:lookup(DeviceId) of
        {error, not_find} ->
            Device =
                #{
                    <<"devaddr">> => SysAddr,
                    <<"name">> => SysType,
                    <<"ip">> => Ip,
                    <<"isEnable">> => true,
                    <<"product">> => ProductId,
                    <<"ACL">> => Acl,
                    <<"status">> => <<"ONLINE">>,
                    <<"brand">> => DevType,
                    <<"devModel">> => <<"城市消防"/utf8>>
                },
            dgiot_device:create_device(Device);
        _ ->
            pass
    end,
    case maps:find(<<"data">>, Map) of
        {ok, Data} ->
            dgiot_task:save_td(ProductId, SysAddr, Data, #{});
        _ -> pass
    end.

equdevice(#{<<"systype">> := SysType, <<"sysaddr">> := SysAddr, <<"equtype">> := EquType, <<"equaddr">> := EquAddr} = Map,
        #{<<"source">> := Ip}, #state{devtype = DevType, app = App}) ->
    CategoryId = dgiot_parse:get_categoryid(1, <<"智慧消防"/utf8>>),
    ProductId = dgiot_parse:get_productid(CategoryId, DevType, EquType),
    Acl = #{<<"ACL">> => #{<<"role:",App/binary>> => #{<<"read">> => true, <<"write">> => true}}},
    case dgiot_product:local(ProductId) of
        {ok, _Product} ->
            pass;
        _ ->
            Prodocut = #{
                <<"name">> => SysType,
                <<"devType">> => DevType,
                <<"desc">> => SysType,
                <<"nodeType">> => 1,
                <<"ACL">> => Acl,
                <<"netType">> => <<"CELLULAR">>,
                <<"category">> => #{<<"objectId">> => CategoryId, <<"__type">> => <<"Pointer">>, <<"className">> => <<"Category">>},
                <<"thing">> => #{},
                <<"productSecret">> => dgiot_utils:random(),
                <<"dynamicReg">> => true},
            case dgiot_parse:create_object(<<"Product">>,Prodocut) of
                {ok, #{<<"objectId">> := _ObjectId}} ->
                    dgiot_product:save(Prodocut);
                _ -> pass
            end
    end,
    DeviceId = dgiot_parse:get_deviceid(ProductId, EquAddr),
    case dgiot_device:lookup(DeviceId) of
        {error, not_find} ->
            SysProductId = dgiot_parse:get_productid(CategoryId, DevType, SysType),
            SysDeviceId = dgiot_parse:get_deviceid(SysProductId, SysAddr),
            Device =
                #{
                    <<"devaddr">> => EquAddr,
                    <<"name">> => EquType,
                    <<"ip">> => Ip,
                    <<"route">> => #{SysAddr => SysDeviceId},
                    <<"isEnable">> => true,
                    <<"product">> => ProductId,
                    <<"ACL">> => Acl,
                    <<"status">> => <<"ONLINE">>,
                    <<"brand">> => DevType,
                    <<"devModel">> => <<"城市消防"/utf8>>
                },
            dgiot_device:create_device(Device);
        _ ->
            pass
    end,
    case maps:find(<<"data">>, Map) of
        {ok, Data} ->
            dgiot_task:save_td(ProductId, SysAddr, Data, #{});
        _ -> pass
    end.

